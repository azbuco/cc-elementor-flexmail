<?php

use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use ElementorPro\Modules\Forms\Classes\Action_Base;
use ElementorPro\Modules\Forms\Classes\Ajax_Handler;
use ElementorPro\Modules\Forms\Classes\Form_Record;

class Cc_Flexmail_Action extends Action_Base
{
    const API_URL = 'https://api.flexmail.eu/contacts';

    public function get_name()
    {
        return 'flexmail';
    }

    public function get_label()
    {
        return 'Flexmail API';
    }

    /**
     * @access public
     * @param Form_Record $record
     * @param Ajax_Handler $ajax_handler
     */
    public function run($record, $ajax_handler)
    {
        $settings = $record->get('form_settings');
        if (!$this->check_required_settings($settings)) {
            return;
        }

        $raw_fields = $record->get('fields');
        $fields = [];
        foreach ($raw_fields as $id => $field) {
            $fields[$id] = $field['value'];
        }

        if (!$this->check_required_fields($settings, $fields)) {
            return;
        }

        $username = $settings['flexmail_user'];
        $password = $settings['flexmail_password'];

        $data = [
            'name' => $fields[$settings['flexmail_name_field']],
            'first_name' => $fields[$settings['flexmail_first_name_field']],
            'email' => $fields[$settings['flexmail_email_field']],
            'source' => $settings['flexmail_source_id'],
        ];

        // az API hívás
        // hát itt csak sötétben tapogatózok...
        // lehet hogy kell pl a language field is
        $result = wp_remote_post(self::API_URL, [
            'method' => 'POST',
            'headers' => array(
                'Authorization' => 'Basic ' . base64_encode($username . ':' . $password),
                'Content-Type' => 'application/json; charset=utf-8',
            ),
            'body' => json_encode($data),
            'data_format' => 'body',
        ]);

        // Lehet hogy a hibakezelésre nem lenne szükség.
        // Mivel az elementornál több akció is van, ami esetleg már lefutott, az itteni hibát lehet szép csendben le kéne nyelni,
        // vagy esetleg az adminnak jelezni levélben.
        if ($result instanceof WP_Error || $result['response']['code'] >= 300) {
            $ajax_handler->add_error_message('Hiba a Flexmail API csatlakozással');
        }
    }

    /**
     * @param Widget_Base $widget
     */
    public function register_settings_section($widget)
    {
        $widget->start_controls_section(
            'section_cc_flexmail',
            [
                'label' => 'Flexmail API',
                'condition' => [
                    'submit_actions' => $this->get_name(),
                ],
            ]
        );

        $widget->add_control(
            'flexmail_user',
            [
                'label' => 'API username',
                'type' => Controls_Manager::TEXT,
                'separator' => 'before',
                'description' => 'Username for the Flexmail REST API.'
            ]
        );

        $widget->add_control(
            'flexmail_password',
            [
                'label' => 'API password',
                'type' => Controls_Manager::TEXT,
                'description' => 'Password for the Flexmail REST API.'
            ]
        );

        $widget->add_control(
            'flexmail_source_id',
            [
                'label' => 'Flexmail Source ID',
                'type' => Controls_Manager::TEXT,
                'separator' => 'before',
                'description' => 'An existing source ID (integer) within the Flexmail system.'
            ]
        );

        $widget->add_control(
            'flexmail_name_field',
            [
                'label' => 'Name Field ID',
                'type' => Controls_Manager::TEXT,
                'separator' => 'before',
            ]
        );

        $widget->add_control(
            'flexmail_first_name_field',
            [
                'label' => 'Firstname Field ID',
                'type' => Controls_Manager::TEXT,
            ]
        );

        $widget->add_control(
            'flexmail_email_field',
            [
                'label' => 'Email Field ID',
                'type' => Controls_Manager::TEXT,
            ]
        );

        $widget->end_controls_section();

    }

    /**
     * @param array $element
     */
    public function on_export($element)
    {
        unset(
            $element['flexmail_user'],
            $element['flexmail_password'],
            $element['flexmail_name_field'],
            $element['flexmail_first_name_field'],
            $element['flexmail_email_field'],
            $element['flexmail_source_id']
        );
    }

    /**
     * @param array $settings
     * @return bool
     */
    public function check_required_settings($settings)
    {
        $required_settings = [
            'flexmail_user',
            'flexmail_password',
            'flexmail_name_field',
            /*
            Nem vagyok biztos benne hogy first_name nélkül a flexmail mit csinál,
            Arra tippelnék, hogy elvan simán a name-mezővel, de ezt ki kéne próbálni
            */
            // 'flexmail_first_name_field',
            'flexmail_email_field',
            'flexmail_source_id'
        ];

        foreach ($required_settings as $id) {
            if (empty($settings[$id])) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $settings
     * @param array $fields
     * @return bool
     */
    public function check_required_fields($settings, $fields)
    {
        if (empty($fields[$settings['flexmail_name_field']])) {
            return false;
        }

        if (empty($fields[$settings['flexmail_email_field']])) {
            return false;
        }

        return true;
    }
}