<?php

/*
Plugin Name: Elementor Flexmail Action
Description: Adding flexmail support for elementor forms.
Version: 1.0
Author: Székely Tamás
*/

use ElementorPro\Plugin;

add_action('elementor_pro/init', function () {
    require_once(__DIR__ . DIRECTORY_SEPARATOR . 'cc-flexmail-action.php');

    $action = new Cc_Flexmail_Action();
    Plugin::instance()->modules_manager->get_modules('forms')->add_form_action($action->get_name(), $action);
});