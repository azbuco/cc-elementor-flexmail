Flexmail REST API plugin Wordpress és Elementor Pro alá
=======================================================

Telepítés
---------

1. Másoljik fel a repository tartalmát egy `cc-elementor-flexmail` mappába a Wordpress wp-content/plugins mappájába.
2. Aktiváljuk a Wordpress admin felületen az `Elementor Flexmail Action` plugint a Bővítmények / Telepített bővítmények menüpontban

Használat
---------

Az Elementor Pro form szerkesztő felületén adjuk hozzá a `Flexmail API` akciót az `Actions After Sumbmit` blokkban.

![img_1.png](img_1.png)

A `Flexmail API` blokkban adjuk meg a Flexmail csatlakozáshoz szükséges adatokat. 

![img_2.png](img_2.png)

A Name/Firstname/Email mezők ID-jét a `Form Fields` blokk egy mezőjét kiválasztva, az `advanced` tabon lehet megnézni.

![img_3.png](img_3.png)

A Source ID a Flexmail admin felületéből kéne hogy kiderüljön, de ezt nem tudom megnézni hozzáférés nélkül.

MEJEGYZÉS: Fogalmam sincs a Flexmail hogy kezeli a name / first_name mezőket. Lehet hogy szét kell szedni minden név inputot.